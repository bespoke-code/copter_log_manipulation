from shutil import copyfileobj
from os import remove
from subprocess import PIPE, call
import pandas as pd
import numpy as np
from scipy import interpolate


def generateSensorLog(log_filepath: str, sensor_log_name: str, sensor_type: str) -> None:
    """
    Extracts specific sensor log data from a global autopilot log file
    and saves them to disk on the location specified in sensor_log_name.
    :param log_filepath: the global (autopilot) log filepath.
    :param sensor_log_name: The filepath where the new sensor log
    :param sensor_type: The name of the specific sensor to build a log for.
    :return: None
    """

    # This command extracts necessary sensor data from the log file and saves them at the given location
    command = ['cat {original_log} | grep "\\b{sensor_type}, " > {sensor_log_name}'.format(
        original_log=log_filepath,
        sensor_type=sensor_type.upper(),
        sensor_log_name=sensor_log_name
        )]

    # Execute the command presented above
    call(command, shell=True, executable='/bin/bash', stdout=PIPE, stderr=PIPE)

    # For a non-blocking way of doing things, one can use this (here - undesirable)
    #   bash_command = Popen(command, shell=True, executable='/bin/bash', stdout=PIPE, stderr=PIPE)
    #   output, error = bash_command.communicate()

    # Open the (new) sensor log file and get the header line
    log_file = open(sensor_log_name, 'r')
    header = log_file.readline()

    # Modify the first line in the sensor log
    start = header.find('TimeUS')
    new_header = "{sensor},".format(sensor=sensor_type) + header[start:]

    # Save the sensor log with the modified header in a temporary file
    temp_log_file = open(sensor_log_name+"1", 'w')
    temp_log_file.write(new_header)
    copyfileobj(log_file, temp_log_file)

    # close both logs
    log_file.close()
    temp_log_file.close()

    # Reopen the temporary and sensor log file with appropriate permissions
    temp_log_file = open(sensor_log_name + "1", 'r')
    log_file = open(sensor_log_name, 'w')

    # Copy the log file contents to the sensor log file
    copyfileobj(temp_log_file, log_file)
    temp_log_file.close()
    log_file.close()

    # delete temporary (transitional) log file
    remove(sensor_log_name + '1')


def interpolateSpline(axis_times, axis_points, interpolated_times):
    """
    Calculates coefficients for a smooth spline from a set of 2D points.
    Interpolates a smooth spline in 2D to the given points.
    :param axis_times: The x-axis points (time)
    :param axis_points: The y-axis points (value)
    :param interpolated_times:
    :return:
    """
    tck = interpolate.splrep(axis_times, axis_points, s=0)
    # return interpolated points
    return interpolate.splev(interpolated_times, tck)


def generateCombinedLog(gps_log_path, imu_log_path, augmented_gps_log_path):
    gps_log = pd.read_csv(gps_log_path, delimiter=',', header=0)
    imu_log = pd.read_csv(gps_log_path, delimiter=',', header=0)
    augmented_log = open(augmented_gps_log_path, mode='w')

    # TODO: Write interpolation tactic

    augmented_log.close()
