import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from math import pi, cos, sin, sqrt

from pixhawkLogManipulator import generateSensorLog, interpolateSpline

# Path Visualisation script.
# Author: Andrej Georgievski <andrej.georgievski@visiondynamix.com>
# Date: 4. July 2018

# Description: The following code (script) reads a CSV-formatted file and transforms WGS-84 coordinates to XYZ.
#              The transformed coordinates form a 3D trajectory which is then plotted using matplotlib.

# Improvements to be done:
# TODO: Add orientation (roll, pitch, yaw) details to path


def interpolateAndPlot(dataSetMain, dataSetToInterpolate, time_label, data_labels, titles, ylabels, timeDiv_sec=1e6):
    dataA_interpolated = interpolateSpline(dataSetMain[time_label], dataSetMain[data_labels[0]], dataSetToInterpolate[time_label])
    dataB_interpolated = interpolateSpline(dataSetMain[time_label], dataSetMain[data_labels[1]], dataSetToInterpolate[time_label])
    dataC_interpolated = interpolateSpline(dataSetMain[time_label], dataSetMain[data_labels[2]] ,dataSetToInterpolate[time_label])

    # Plot interpolated and original data
    plt.subplot(3,1,1)
    plt.plot(dataSetToInterpolate[time_label]/timeDiv_sec, dataA_interpolated, 'r.')
    plt.plot(dataSetMain[time_label]/timeDiv_sec, dataSetMain[data_labels[0]], 'b.')
    plt.title(titles[0])
    plt.ylabel(ylabels[0])
    plt.xlabel('Time (s)')
    plt.subplot(3,1,2)
    plt.plot(dataSetToInterpolate[time_label]/timeDiv_sec, dataB_interpolated, 'r.')
    plt.plot(dataSetMain[time_label]/timeDiv_sec, dataSetMain[data_labels[1]], 'b.')
    plt.title(titles[1])
    plt.ylabel(ylabels[1])
    plt.xlabel('Time (s)')
    plt.subplot(3,1,3)
    plt.plot(dataSetToInterpolate[time_label]/timeDiv_sec, dataC_interpolated, 'r.')
    plt.plot(dataSetMain[time_label]/timeDiv_sec, dataSetMain[data_labels[2]], 'b.')
    plt.title(titles[2])
    plt.ylabel(ylabels[2])
    plt.xlabel('Time (s)')
    plt.show()


def wgs2geocentric(lat: float, lon: float, alt: float):
    """
    Transforms WGS84 coordinates to geocentric XYZ (Cartesian 3D).
    :param lat: target point's latitude in degrees (decimal)
    :param lon: target point's longitude in degrees (decimal)
    :param alt: target point's altitude in degrees (decimal)
    :return: transformed X Y Z cartesian coordinates of the target point
    """

    L = lon * (1E-7 * pi / 180)
    B = lat * (1E-7 * pi / 180)
    H = alt * (1E-3) # TODO: alt * a + b alt: 265 196 mm ?

    a = 6378137.00000 # 6'378'137.00000
    b = 6356752.31425 # 6'356'752.31425
    c2 = 1 - (b / a)**2 # 6.69437999E-3

    cosB = cos(B)
    sinB = sin(B)

    N = a / sqrt(1 - c2 * sinB * sinB)
    NH = N + H

    x = NH * cosB * cos(L)
    y = NH * cosB * sin(L)
    z = (N * (1 - c2) + H) * sinB

    return x,y,z


def magnitude(x, y, z):
    """
    Vector magnitude calculation.
    :param x: x value (length)
    :param y: y value (length)
    :param z: z value (length)
    :return: magnitude (length) of the radius vector
    """
    return sqrt(x**2+y**2+z**2)


def transform3D(tx, ty, tz, roll, pitch, yaw):
    """
    Calculates and returns the homogeneous transformation from
    the local coordinate system to the global one.
    :param tx: translation on x-axis.
    :param ty: translation on y-axis.
    :param tz: translation on z-axis.
    :param roll: roll angle in rad (around x-axis)
    :param pitch: pitch angle in rad  (around y-axis)
    :param yaw: yaw angle in rad (around z-axis)
    :return: 4x4 homogeneous transformation (drone 2 world)
    """
    # Transform the coordinates using extrinsic rotations
    rot_x = np.array([[1,         0,          0, 0],
                      [0, cos(roll), -sin(roll), 0],
                      [0, sin(roll),  cos(roll), 0],
                      [0,         0,          0, 1]])

    rot_y = np.array([[cos(pitch),  0, sin(pitch), 0],
                      [0,           1,          0, 0],
                      [-sin(pitch), 0, cos(pitch), 0],
                      [0,           0,          0, 1]])

    rot_z = np.array([[cos(yaw), -sin(yaw), 0, 0],
                      [sin(yaw),  cos(yaw), 0, 0],
                      [0,                0, 1, 0],
                      [0,                0, 0, 1]])
    trans = np.array([[0, 0, 0, tx],
                      [0, 0, 0, ty],
                      [0, 0, 0, tz],
                      [0, 0, 0,  0]])

    return np.matmul(rot_z, np.matmul(rot_y, rot_x)) + trans


def view(xs, ys, zs, rolls, pitches, yaws):
    # Plot path in 3D (or color-coded 2D)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(xs, ys, zs, 'r.', label='Drone flight path')

    # plot orientations
    o = np.array([[0], [0], [0], [1]]) # origin
    i = np.array([[1e-6], [0], [0], [1]]) # x-axis endpoint
    j = np.array([[0], [1e-6], [0], [1]]) # y-axis endpoint
    k = np.array([[0], [0], [1e-6], [1]]) # z-axis endpoint
    for index in range(len(xs)):
        if index%100 == 0:
            trans = transform3D(xs[index], ys[index], zs[index],
                                rolls[index], pitches[index], yaws[index])

            # transform end points of each axis + the origin
            i_trans = np.matmul(trans, i)
            j_trans = np.matmul(trans, j)
            k_trans = np.matmul(trans, k)
            o_trans = np.matmul(trans, o)

            # plot each axis in the 3D viewer
            for axis_colour in ([i_trans, 'r'], [j_trans, 'g'], [k_trans, 'b']):
                x_points = [o_trans[0,0], axis_colour[0][0,0]]
                y_points = [o_trans[1,0], axis_colour[0][1,0]]
                z_points = [o_trans[2,0], axis_colour[0][2,0]]
                ax.plot(x_points, y_points, z_points, axis_colour[1])

    ax.legend()
    plt.show()


if __name__ == '__main__':
    # Define global names for required parameters, since they appear under different names in different logs
    lat_ = 'Lat'
    lon_ = 'Lng'
    alt_ = 'Alt'
    time_ = 'TimeUS'
    gyrox_ = 'GyrX'
    gyroy_ = 'GyrY'
    gyroz_ = 'GyrZ'
    accx_ = 'AccX'
    accy_ = 'AccY'
    accz_ = 'AccZ'
    roll_ = 'Roll'
    pitch_ = 'Pitch'
    yaw_ = 'Yaw'

    generateSensorLog('testData/log_berovo_goPro.txt',
                      'testData/log_gps_berovo_goPro.csv',
                      'GPS')
    generateSensorLog('testData/log_berovo_goPro.txt',
                      'testData/log_imu_berovo_goPro.csv',
                      'IMU')
    generateSensorLog('testData/log_berovo_goPro.txt',
                      'testData/log_ahr2_berovo_goPro.csv',
                      'AHR2')

    ########################################
    # Read IMU, GPS and Image data from disk
    imu_data = pd.read_csv('testData/log_imu_berovo_goPro.csv', delimiter=',', header=0)
    gps_data = pd.read_csv('testData/log_gps_berovo_goPro.csv', delimiter=',', header=0)
    ahr2_data = pd.read_csv('testData/log_ahr2_berovo_goPro.csv', delimiter=',', header=0)

    # Format the GPS data parameters as decimal degrees (lat, lon) and meters (alt)

    # Interpolate GPS data points
    interpolateAndPlot(gps_data, imu_data, time_,
                       data_labels=[lat_, lon_, alt_],
                       titles=['GPS lat (interpolated)', 'GPS lon (interpolated)', 'GPS alt (interpolated)'],
                       ylabels=['GPS lat', 'GPS lon', 'GPS alt'])

    # Interpolate Gyroscope data
    interpolateAndPlot(imu_data, gps_data, time_,
                       data_labels=[gyrox_, gyroy_, gyroz_],
                       titles=['IMU gyro x (interpolated in red)', 'IMU gyro Y (interpolated in red)', 'IMU gyro z (interpolated in red)'],
                       ylabels=['Gyro X', 'Gyro Y', 'Gyro Z'])

    # Interpolate Accelerometer data
    interpolateAndPlot(imu_data, gps_data, time_,
                       data_labels=[accx_, accy_, accz_],
                       titles=['IMU acc x (interpolated in red)', 'IMU acc Y (interpolated in red)', 'IMU acc z (interpolated in red)'],
                       ylabels=['Acc X', 'Acc Y', 'Acc Z'])

    interpolateAndPlot(gps_data, ahr2_data, time_,
                       data_labels=[lat_, lon_, alt_],
                       titles=['AHR data lat', 'AHR data lon', 'AHR data alt'],
                       ylabels=[lat_, lon_, alt_])

    interpolateAndPlot(ahr2_data, gps_data, time_,
                       data_labels=[roll_, pitch_, yaw_],
                       titles=['AHR DATA roll', 'AHR data pitch', 'AHR data yaw'],
                       ylabels=[lat_, lon_, alt_])


    # Transform GPS elliptic (lat,lon,alt) to geocentric (x,y,z)
    for index in np.arange(0,gps_data.shape[0]):
        x,y,z = wgs2geocentric(gps_data.iloc[index][lat_],
                               gps_data.iloc[index][lon_],
                               gps_data.iloc[index][alt_])
        gps_data.at[index,lat_] = x
        gps_data.at[index,lon_] = y
        gps_data.at[index,alt_] = z

    for index in np.arange(0,ahr2_data.shape[0]):
        x,y,z = wgs2geocentric(ahr2_data.iloc[index][lat_],
                               ahr2_data.iloc[index][lon_],
                               ahr2_data.iloc[index][alt_])
        ahr2_data.at[index,lat_] = x
        ahr2_data.at[index,lon_] = y
        ahr2_data.at[index,alt_] = z

    xa = ahr2_data.as_matrix(columns=[lat_])
    xa = np.reshape(xa, xa.shape[0])
    ya = ahr2_data.as_matrix(columns=[lon_])
    ya = np.reshape(ya, ya.shape[0])
    za = ahr2_data.as_matrix(columns=[alt_])
    za = np.reshape(za, za.shape[0])

    # Prepare data for plotting
    x = gps_data.as_matrix(columns=[lat_])
    x = np.reshape(x, x.shape[0])
    y = gps_data.as_matrix(columns=[lon_])
    y = np.reshape(y, y.shape[0])
    z = gps_data.as_matrix(columns=[alt_])
    z = np.reshape(z, z.shape[0])

    roll = ahr2_data.as_matrix(columns=[roll_])
    roll = np.reshape(roll, roll.shape[0])/(pi/180.)
    pitch = ahr2_data.as_matrix(columns=[pitch_])
    pitch = np.reshape(pitch, pitch.shape[0])/(pi/180.)
    yaw = ahr2_data.as_matrix(columns=[yaw_])
    yaw = np.reshape(yaw, yaw.shape[0])/(pi/180.)

    length = min(x.shape[0], roll.shape[0])
    view(x[0:length], y[0:length], z[0:length], roll[0:length], pitch[0:length], yaw[0:length])
