import pandas as pd
import numpy as np
import pixhawkLogManipulator as px4manip
from glob import glob
from pathVisualizer import view, wgs2geocentric
from math import pi, cos, sin, sqrt

if __name__ == '__main__':
    # Prepare / Generate separate logs for GPS, IMU, AHR2
    lat_ = 'Lat'
    lon_ = 'Lng'
    alt_ = 'Alt'
    time_ = 'TimeUS'
    roll_ = 'Roll'
    pitch_ = 'Pitch'
    yaw_ = 'Yaw'

    px4manip.generateSensorLog('testData/log_berovo_goPro.txt',
                               'testData/log_gps_berovo_goPro.csv',
                               'GPS')
    px4manip.generateSensorLog('testData/log_berovo_goPro.txt',
                               'testData/log_imu_berovo_goPro.csv',
                               'IMU')
    px4manip.generateSensorLog('testData/log_berovo_goPro.txt',
                               'testData/log_ahr2_berovo_goPro.csv',
                               'AHR2')

    imu_data = pd.read_csv('testData/log_imu_berovo_goPro.csv', delimiter=',', header=0)
    gps_data = pd.read_csv('testData/log_gps_berovo_goPro.csv', delimiter=',', header=0)
    ahr2_data = pd.read_csv('testData/log_ahr2_berovo_goPro.csv', delimiter=',', header=0)

    min_imu_time = imu_data.min()[time_]
    min_gps_time = gps_data.min()[time_]
    min_ahr2_time = ahr2_data.min()[time_]

    max_imu_time = imu_data.max()[time_]
    max_gps_time = gps_data.max()[time_]
    max_ahr2_time = ahr2_data.max()[time_]

    min_time = np.min([min_ahr2_time, min_gps_time, min_imu_time])
    max_time = np.max([max_ahr2_time, max_gps_time, max_imu_time])

    # Grab the list of frames from disk
    frames_location_on_disk = '/home/vdx2/Videos/berovo-gopro/img_rect/'
    frames = glob(frames_location_on_disk + '*.jpg')
    frames = [frame.split('/')[-1] for frame in frames]
    frames.sort()

    # Generate frame time points
    framerate = 60 #59.94 ?
    frames_count = len(frames)
    first_image_time = 89000000 #microseconds
    delta_t_cam_to_data = -12500000 #microseconds

    dt = int((1/framerate)*1E6)
    start_t = first_image_time + delta_t_cam_to_data
    end_t = start_t + frames_count * dt
    frame_timeUS = np.int_(np.arange(start_t, end_t, dt))

    # save time, image_name in a CSV file w/ Pandas.
    frames_timing_data = pd.DataFrame(np.column_stack((frame_timeUS, frames)),
                                      columns=[time_, 'Frame'])
    frames_timing_data[time_] = frames_timing_data[time_].astype(np.int_)
    frames_timing_data.to_csv('testData/frame_timings.csv', index=False)

    ###############################################
    # interpolate GPS positions at each frame point
    lat_values = px4manip.interpolateSpline(gps_data[time_], gps_data[lat_], frame_timeUS)
    lon_values = px4manip.interpolateSpline(gps_data[time_], gps_data[lon_], frame_timeUS)
    alt_values = px4manip.interpolateSpline(gps_data[time_], gps_data[alt_], frame_timeUS)

    # interpolate the ATTITUDE at each frame point
    roll_values = px4manip.interpolateSpline(ahr2_data[time_], ahr2_data[roll_], frame_timeUS)
    pitch_values = px4manip.interpolateSpline(ahr2_data[time_], ahr2_data[pitch_], frame_timeUS)
    yaw_values = px4manip.interpolateSpline(ahr2_data[time_], ahr2_data[yaw_], frame_timeUS)

    # Get everything in a pandas dataframe.
    interpolated_sensor_data = pd.DataFrame(np.column_stack((frame_timeUS,
                                                             lat_values,
                                                             lon_values,
                                                             alt_values,
                                                             roll_values,
                                                             pitch_values,
                                                             yaw_values)),
                                            columns=(time_, lat_, lon_, alt_, roll_, pitch_, yaw_))

    # SORT by time.
    interpolated_sensor_data.sort_values(by=[time_])
    interpolated_sensor_data[time_] = interpolated_sensor_data[time_].astype(np.int_)

    # save Time, GPS, ATTITUDE points in a CSV file w/ Pandas.
    interpolated_sensor_data.to_csv('testData/interpolated_sensor_data.csv', index=False)


    #################################################
    # for Stefan and Stole
    fss = pd.DataFrame(np.column_stack((frames, frame_timeUS)),
                       columns=['Frame', time_])
    fss[time_] = frames_timing_data[time_].astype(np.int_)

    # Get everything in a pandas dataframe.
    gpsd = pd.DataFrame(np.column_stack((frame_timeUS,
                                         lat_values,
                                         lon_values,
                                         alt_values
                                         )),
                        columns=('time', 'lat', 'lon', 'alt'))
    imud = pd.DataFrame(np.column_stack((frame_timeUS,
                                         roll_values,
                                         pitch_values,
                                         yaw_values)),
                        columns=('time', 'roll', 'pitch', 'yaw'))

    # SORT by time.
    gpsd.sort_values(by=['time'])
    gpsd['time'] = gpsd['time'].astype(np.int_)
    imud.sort_values(by=['time'])
    imud['time'] = imud['time'].astype(np.int_)

    # save Time, GPS, ATTITUDE points in a CSV file w/ Pandas.
    fss.to_csv('testData/stefan/berovo/tstamps.csv', index=False, header=False)
    gpsd.to_csv('testData/stefan/berovo/gps_data.csv', index=False)
    imud.to_csv('testData/stefan/berovo/imu_data.csv', index=False)
    # /end for Stefan and Stole
    #################################################

    ############################################################
    # VIEW RESULTS
    # Transform GPS elliptic (lat,lon,alt) to geocentric (x,y,z)
    for index in np.arange(0,interpolated_sensor_data.shape[0]):
        x,y,z = wgs2geocentric(interpolated_sensor_data.iloc[index][lat_],
                               interpolated_sensor_data.iloc[index][lon_],
                               interpolated_sensor_data.iloc[index][alt_])
        interpolated_sensor_data.at[index,lat_] = x
        interpolated_sensor_data.at[index,lon_] = y
        interpolated_sensor_data.at[index,alt_] = z

        # Prepare data for plotting
        x = interpolated_sensor_data.as_matrix(columns=[lat_])
        x = np.reshape(x, x.shape[0])
        y = interpolated_sensor_data.as_matrix(columns=[lon_])
        y = np.reshape(y, y.shape[0])
        z = interpolated_sensor_data.as_matrix(columns=[alt_])
        z = np.reshape(z, z.shape[0])

        roll = interpolated_sensor_data.as_matrix(columns=[roll_])
        roll = np.reshape(roll, roll.shape[0]) / (pi / 180.)
        pitch = interpolated_sensor_data.as_matrix(columns=[pitch_])
        pitch = np.reshape(pitch, pitch.shape[0]) / (pi / 180.)
        yaw = interpolated_sensor_data.as_matrix(columns=[yaw_])
        yaw = np.reshape(yaw, yaw.shape[0]) / (pi / 180.)

    view(x,y,z,roll,pitch,yaw)
    # /end VIEW RESULTS
    ############################################################
